class SearchController < ApplicationController

before_action :search, only:[:index]


    def index
        render "articles/index"
    end

    private

    def search
      like_search = "%#{params[:search]}%"
      @articles = Article.includes(:comments).where('title LIKE :query',query: like_search)
    end


end
