class ArticlesController < ApplicationController

    attr_accessor = :title, :content

    before_action :authenticate_user!,
                  only: [:new, :create, :edit, :update, :destroy]

    before_action :find_article, only:[:show,:edit,:destroy,:update]


    def index
        #UPDATE --
        @articles = Article.all
    end

    def show

    end

    def edit
    end

    def create
        @article = Article.new(parameters)
        if  @article.save
            redirect_to article_path(@article.slug), :notice => " L'article a bien été crée"
        else
           render 'new'
        end

    end

    def new
        @article = Article.new
    end



    def update
        if @article.update(parameters)
            redirect_to articles_path, :notice => " L'article a bien été mis a jour"
        else
            render "edit"
        end
    end

    def destroy
        @article.destroy
        redirect_to ({action:'index'}), notice:"article supprimé"
    end


    private

    def parameters
        params[:article][:user_id] = current_user.id
        params.require(:article).permit(:title, :content, :user_id,:slug)
    end

    def find_article
        @article = Article.includes([:comments]).find_by(slug: params[:slug])
    end


end
