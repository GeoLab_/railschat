class CommentsController < ApplicationController

  def index
  end

  def new
  end

  def create
    @article = Article.where('slug = :slug',slug: params[:article_slug]).first
    @comment = Comment.new(parameters).save

    redirect_back(fallback_location: root_path)
  end

  def update
  end

  def destroy
  end

  private

  def parameters
      params[:comment][:user_id] = current_user.id
      params[:comment][:article_id] = @article.id
      return params[:comment].permit([:comment,:user_id,:article_id])
  end

end
