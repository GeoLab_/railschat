class Article < ApplicationRecord

    validates :title, length:{minimum:10, too_short: "%{count} characters is the minimum allowed" }
    validates :content, length:{minimum:10, too_short: "%{count} characters is the minimum allowed" }

    before_validation :slugify

    has_many :comments
    belongs_to :user

    private

    def slugify
        self.slug = self.title.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
    end

end
