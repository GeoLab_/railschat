Rails.application.routes.draw do

#GET------------articles -----------#index ----- articles_path
#GET------------articles/new -------#new ------- new_article_path
#POST-----------articles -----------#create ---- articles_path
#GET -----------articles/:id -------#show ------ article_path(:id)
#GET -----------articles/:id/edit --#edit ------ edit_article_path(:id)
#PATCH/PUT -----articles/:id -------#update ---- article_path(:id)
#DELETE --------articles/:id -------#destroy --- article_path(:id)
#
    devise_for :users

    resources :articles, param: :slug do
      resources :comments
    end

    resources :categories
    post '/search', to: 'search#index', as: 'search'
    get '/result', to: 'search#result', as: 'result'
    get '/locator', to: 'locator#index', as: 'article_locator'

    root 'home#index'

end
